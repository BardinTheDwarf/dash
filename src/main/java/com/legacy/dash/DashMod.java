package com.legacy.dash;

import java.util.logging.Logger;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(DashMod.MODID)
public class DashMod
{
	public static final String NAME = "Dash";
	public static final String MODID = "dash";
	public static Logger LOGGER = Logger.getLogger(MODID);

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MODID, name);
	}

	public static String find(String name)
	{
		return MODID + ":" + name;
	}

	public DashMod()
	{
		DistExecutor.safeRunWhenOn(Dist.CLIENT, () -> () ->
		{
			MinecraftForge.EVENT_BUS.register(new DashEntityEvents());
			FMLJavaModLoadingContext.get().getModEventBus().addListener(DashClient::clientInit);
		});
	}

	@OnlyIn(Dist.CLIENT)
	static class DashClient
	{
		@OnlyIn(Dist.CLIENT)
		public static net.minecraft.client.settings.KeyBinding DASH_KEYBIND = new net.minecraft.client.settings.KeyBinding("dash.key.dash", 82, "key.categories.movement");

		@SubscribeEvent
		public static void clientInit(FMLClientSetupEvent event)
		{
			DashKeyRegistry dashKeyRegistry = new DashKeyRegistry();
			MinecraftForge.EVENT_BUS.register(dashKeyRegistry);
			dashKeyRegistry.register(DASH_KEYBIND);
		}
	}
}
